import Default from "./default"
import Mobile from "./mobile"
import None from "./none"
import Blank from "./blank"

export default {
    default: Default,
    mobile: Mobile,
    none: None,
    blank: Blank,
}